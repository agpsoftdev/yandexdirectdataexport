
import requests, json, csv, collections, os, io
from requests.exceptions import ConnectionError
from google.cloud import bigquery
from time import sleep

fields = ['Date', 'CampaignType', 'CampaignId', 'CampaignName', 'AdGroupName', 'Impressions', 'Clicks', 'Cost', 'AvgImpressionPosition', 'AvgCpc', 'Ctr']

keyFile = ''
datasetName = ''
tableName = ''
yandexAuthConfig = {}

def reportToJSON(report = ''): 
  data_dumps = ''
  reader = csv.DictReader(io.StringIO(report), fieldnames = fields, dialect='excel-tab')
  
  for row in reader:
    if row['AvgImpressionPosition'] == '--':
      row['AvgImpressionPosition'] = '' 
      
    if row['AvgCpc'] == '--':
      row['AvgCpc'] = 0    
    
    row['AvgCpc'] = float(row['AvgCpc']) / 1000000 
    row['Cost'] = float(row['Cost']) / 1000000
      
    data_dumps += json.dumps(row) + os.linesep
    
  return data_dumps

def load_to_gbq(data):
  """
      Loading data to BigQuery using *bq_configuration* settings.
  """
  #client = bigquery.Client(project = bq_configuration["project_id"])
  client = bigquery.Client.from_service_account_json(keyFile)
  dataset_ref = client.dataset(datasetName)
  table_ref = dataset_ref.table(tableName)

  # determine uploading options
  job_config = bigquery.LoadJobConfig()
  job_config.write_disposition = 'WRITE_TRUNCATE'
  job_config.source_format = "NEWLINE_DELIMITED_JSON"
  job_config.autodetect = True

  load_job = client.load_table_from_file(
    io.StringIO(data),
    table_ref,
    job_config = job_config)  # API request
  print('Starting job {}'.format(load_job.job_id))

  load_job.result()  # Waits for table load to complete.
  
  print('Job finished.')

def getreport(yandexAuthConfig): 
  url = 'https://api.direct.yandex.com/json/v5/reports'
  
  headers = {
    # OAuth token. The word Bearer must be used
    "Authorization": "Bearer " + yandexAuthConfig['token'],
    # Login of the advertising agency client
    "Client-Login": yandexAuthConfig['clientLogin'],
    # Language for response messages
    "Accept-Language": "en",
    # Mode for report generation
    "processingMode": "auto",
    # Format for monetary values in the report
    # "returnMoneyInMicros": "false",
    # Don't include the row with the report name and date range in the report
    "skipReportHeader": "true",
    # Don't include the row with column names in the report
    "skipColumnHeader": "true",
    # Don't include the row with the number of statistics rows in the report
    "skipReportSummary": "true"
  }  
  
  body = {
      "params": {
          "SelectionCriteria": {
#              "DateFrom": "2019-07-01",
#              "DateTo": "2019-08-08"
          },
          "FieldNames": fields,
          "ReportName": 'CUSTOM_REPORT:LAST_365_DAYS',
          "ReportType": "CUSTOM_REPORT",
          "DateRangeType": "LAST_365_DAYS",
          "Format": "TSV",
          "IncludeVAT": "NO",
          "IncludeDiscount": "NO"
      }
  }  
  
  body = json.dumps(body, indent=4)
  
  while True:
    try:
      req = requests.post(url, body, headers=headers)
      req.encoding = 'utf-8'  # Mandatory response processing in UTF-8
      if req.status_code == 400:
        print("Invalid request parameters, or the report queue is full")
        print("RequestId: {}".format(req.headers.get("RequestId", False)))
        print("JSON code for the request: {}".format(body))
        print("JSON code for the server response: \n{}".format(req.json()))
        break
      elif req.status_code == 200:
        print("Report created successfully")
        print("RequestId: {}".format(req.headers.get("RequestId", False)))
        data = reportToJSON(req.text)
        load_to_gbq(data);
        print("Report contents: \n{}".format(req.text))
        break
      elif req.status_code == 201:
        print("Report successfully added to the offline queue")
        retryIn = int(req.headers.get("retryIn", 60))
        print("Request will be resent in {} seonds".format(retryIn))
        print("RequestId: {}".format(req.headers.get("RequestId", False)))
        sleep(retryIn)
      elif req.status_code == 202:
        print("Report is being created in offline mode")
        retryIn = int(req.headers.get("retryIn", 60))
        print("Request will be resent in {} seconds".format(retryIn))
        print("RequestId:  {}".format(req.headers.get("RequestId", False)))
        sleep(retryIn)
      elif req.status_code == 500:
        print("Error occurred when creating the report. Please repeat the request again later")
        print("RequestId: {}".format(req.headers.get("RequestId", False)))
        print("JSON code for the server's response: \n{}".format(req.json()))
        break
      elif req.status_code == 502:
        print("Exceeded the server limit on report creation time.")
        print("Please try changing the request parameters: reduce the time period and the amount of data requested.")
        print("JSON code for the request: {}".format(body))
        print("RequestId: {}".format(req.headers.get("RequestId", False)))
        print("JSON code for the server's response: \n{}".format(req.json()))
        break
      else:
        print("Unexpected error")
        print("RequestId:  {}".format(req.headers.get("RequestId", False)))
        print("JSON code for the request: {}".format(body))
        print("JSON code for the server's response: \n{}".format(req.json()))
        break
  
    # Error handling if the connection with the Yandex.Direct API server wasn't established
    except ConnectionError:
      # In this case, we recommend repeating the request again later
      print("Error connecting to the Yandex.Direct API server")
      # Forced exit from loop
      break
  
    # If any other error occurred
    except Exception as inst:
      # In this case, you should analyze the application's actions
      print(inst)
      # Forced exit from loop
      break  

getreport(yandexAuthConfig)


print("All done");
